import imaplib
import email
import re
import pytz
from datetime import datetime
from collections import OrderedDict

from jinja2 import Environment, PackageLoader
env = Environment(loader=PackageLoader('templates', ''))

def extract_body(payload):
    if isinstance(payload,str):
        return payload
    else:
        return '\n'.join([extract_body(part.get_payload()) for part in payload])

def coordinates():
    re_latitude = re.compile('^[\w\s]*Latitude\:(\-*[0-9]+\.*[0-9]*)[\w\s\-]*$',re.MULTILINE)
    re_longitude = re.compile('^[\w\s\:\.0-9\-]*Longitude\:(\-*[0-9]+\.*[0-9]*)[\w\s]*$',re.MULTILINE)
    re_time = re.compile('^[\w\s\:\.0-9\-]*GPS\ location\ Date\/Time\:([0-9]+\/[0-9]+\/[0-9]+\s*[0-9]+\:[0-9]+\:[0-9]+)\s*([A-Z]+)[\w\s]*$',re.MULTILINE)
    
    conn = imaplib.IMAP4_SSL("imap.gmail.com", 993)
    conn.login("alistril", "aekx68eb")
    conn.select()
    typ, data = conn.search(None, 'UNSEEN')
    beacon_dict = {}
    try:
        for num in data[0].split():
            typ, msg_data = conn.fetch(num, '(RFC822)')
            for response_part in msg_data:
                if isinstance(response_part, tuple):
                    msg = email.message_from_string(response_part[1])
                    subject=msg['subject']                   
                    payload=msg.get_payload()
                    body=extract_body(payload)
                    #res = re_longitude.match( body )

                    res = re_latitude.match(body)
                    if res:
                        latitude = float(res.group(1))
                    else:
                        raise Exception("no match for latitude")
                    
                    res = re_longitude.match(body)
                    if res:
                        longitude = float(res.group(1))
                    else:
                        raise Exception("no match for longitude")
                    
                    res = re_time.match(body)
                    if res:
                        time = res.group(1)
                        time_zone = res.group(2)
                        timezone = pytz.timezone("America/Vancouver")

                        date_time = datetime.strptime(time,"%m/%d/%Y %H:%M:%S")                        
                    else:
                        raise Exception("no match for time")
                    
                    beacon_dict.update({date_time : {'latitude' : latitude, 'longitude' : longitude}})
    
            #typ, response = conn.store(num, '+FLAGS', r'(\Seen)')
    finally:
        try:
            conn.close()
        except:
            pass
        conn.logout()
        return OrderedDict(sorted(beacon_dict.items(),key=lambda t: t[0]))
    
firstcoord = None
coords = coordinates()

template = env.get_template('basic.html')
print template.render(beacon_dict=coords)
